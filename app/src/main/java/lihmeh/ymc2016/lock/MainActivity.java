package lihmeh.ymc2016.lock;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void playBtnClick (View btn) {
        Integer size = null;
        try {
            size = Integer.valueOf(((EditText)findViewById(R.id.sizeEdit)).getText().toString());
        } catch (Exception ex) {}

        if ((size==null)||(size<2)||(size>8)) {
            Toast.makeText(this,"Please enter a correct size.  2 <= size <= 8",Toast.LENGTH_LONG);
            return;
        }

        Intent intent = new Intent(this, GameActivity.class);
        intent.putExtra("size", size);
        startActivity(intent);
    }
}
