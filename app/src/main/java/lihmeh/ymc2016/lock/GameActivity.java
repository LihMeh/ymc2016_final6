package lihmeh.ymc2016.lock;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.Random;

public class GameActivity extends AppCompatActivity implements ImageButton.OnClickListener {

    private int size;

    GridLayout grid;
    boolean[] data;
    ImageButton[] btns;

    int turns = 0;

    Solver solver = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        size = getIntent().getIntExtra("size",5);
        btns = new ImageButton[size*size];


        grid = (GridLayout)findViewById(R.id.gridlayout);
//        GridLayout.LayoutParams params = new GridLayout.LayoutParams();
//        params.width = GridLayout.LayoutParams.MATCH_PARENT;
//        params.height = GridLayout.LayoutParams.MATCH_PARENT;
//        params.setGravity(Gravity.FILL);
//        grid.setLayoutParams(params);
        grid.setColumnCount(size);
        grid.setRowCount(size);
        for (int r=0;r<size;r++) {
            for (int c=0;c<size;c++) {

                GridLayout.LayoutParams position = new GridLayout.LayoutParams(GridLayout.spec(r), GridLayout.spec(c));
                position.width = 70;
                position.height = 70;
                //position.setGravity(Gravity.CENTER);
                ImageButton btn = new ImageButton(this);
                btn.setOnClickListener(this);
                grid.addView(btn, position);
                btns[r*size+c] = btn;
            }
        }
        initGame();
    }

    @Override
    public View onCreateView(String name, Context context, AttributeSet attrs) {
        return super.onCreateView(name, context, attrs);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (size%2==0) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.gamemenu, menu);
            return true;
        }
        else {
            return super.onCreateOptionsMenu(menu);
        }
    }

    private boolean won() {
        for (int i=0;i<data.length;i++) {
            if (!data[i]) return false;
        }
        return true;
    }


    private void initGame() {
        data = new boolean[size*size];
        Random rand = new Random();
        {
            for (int r = 0; r < size; r++) {
                for (int c = 0; c < size; c++) {
                    data[r * size + c] = rand.nextBoolean();
                }
            }
        }while (won());

        updateButtons();

    }


    private void updateButtons() {
        for (int i=0;i<data.length;i++) {
            btns[i].setImageResource(
                    data[i] ? android.R.drawable.presence_online : android.R.drawable.presence_invisible
            );
        }
    }

    private void toggle(int row, int col) {
        data[row*size+col] = !data[row*size+col];
    }

    private int findBtnIndex (View btn) {
        for (int i=0; i< btns.length; i++) {
            if (btns[i] == btn) {
                return i;
            }
        }
        return -1;
    }


    public void press(int row, int col) {

        turns++;

        for (int i=0;i<size;i++) {
            toggle(row,i);
            toggle(i,col);
        }
        toggle(row,col);
        updateButtons();

        if (won()) handleVictory();
    }

    @Override
    public void onClick(View view) {

        int idx = findBtnIndex(view);
        if (idx<0) return;

        solver = null;


        int row = idx / size;
        int col = idx % size;

        press(row, col);


    }

    private void handleVictory() {
        Intent intent = new Intent(this,PlayAgainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra("size",size);
        intent.putExtra("turns", turns);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==R.id.help) {
            if (solver==null) {
                solver = new Solver(data,size);
            }
            Solver.Step step = solver.getNextStep();
            press(step.row, step.col);
            btns[step.row*size+step.col].setImageResource(android.R.drawable.presence_busy);
            return true;
        }
        else {
            return super.onOptionsItemSelected(item);
        }
    }
}
