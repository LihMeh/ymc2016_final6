package lihmeh.ymc2016.lock;

import java.util.ArrayDeque;
import java.util.Queue;

/**
 * Created by mihailpolovnev on 26/11/16.
 */

public class Solver {
    Queue<Step> steps;
    public Solver(boolean[] data, int size) {
        steps = new ArrayDeque<Step>();

        for (int currentRow = 0; currentRow < size; currentRow++) {
            for (int currentCol=0;currentCol < size; currentCol++) {
                if (data[currentRow*size+currentCol]) continue;

                for (int c=0;c<size;c++) {
                        if (c==currentCol) continue;
                        steps.add(new Step(currentRow, c));
                    }

                for (int r=0;r<size;r++) {
                    if (r==currentRow) continue;
                    steps.add(new Step(r,currentCol));
                }


               steps.add(new Step(currentRow, currentCol));
            }
        }
    }

    public static class Step {
        public int row;
        public int col;
        public Step(int row, int col) {
            this.row = row;
            this.col = col;
        }
    }

    public Step getNextStep() {
        return steps.poll();
    }
}
