package lihmeh.ymc2016.lock;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class PlayAgainActivity extends AppCompatActivity {

    int size;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_again);

        size = getIntent().getIntExtra("size",5);

        int turns = getIntent().getIntExtra("turns",-1);
        ((TextView)findViewById(R.id.stepsView)).setText(String.valueOf(turns));
    }

    public void playAgainClick(View view) {
        Intent intent = new Intent(this,GameActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra("size",size);
        startActivity(intent);
    }
}
